<?php
/*Migrate CCTM fields to ACF (this is specific to  PT )
include "wp-load.php";

/*
$posts = new WP_Query('post_type=any&posts_per_page=-1&post_status=publish');
$posts = $posts->posts;

*/
global $wpdb;
$posts = $wpdb->get_results("
    SELECT ID,post_type,post_title
    FROM {$wpdb->posts}
    WHERE post_type  IN ('media-watch','research','training','learning')
");



header('Content-type:text/plain');
$count=0;


foreach($posts as $post) {
	
	
	$count++;
	echo "\n$count\t{$post->ID}\t{$post->post_type}\t{$post->post_title}";
	switch ($post->post_type) {
 
        case 'media-watch':
			if (get_post_meta( $post->ID, 'article_author' )) {
				if ( ! add_post_meta( $post->ID, '_article_author', 'field_586fb7a7a9d31', true ) )  
					update_post_meta( $post->ID, '_article_author', 'field_586fb7a7a9d31');
			}
			if (get_post_meta( $post->ID, 'linkurl' )) {
				if ( ! add_post_meta( $post->ID, '_linkurl', 'field_586fb7e2a9d32', true ) )  
					update_post_meta( $post->ID, '_linkurl', 'field_586fb7e2a9d32');
			}
			if (get_post_meta( $post->ID, 'published' )) {
				if ( ! add_post_meta( $post->ID, '_published', 'field_586fb849a9d33', true ) )  
					update_post_meta( $post->ID, '_published', 'field_586fb849a9d33');
			}
			if (get_post_meta( $post->ID, 'source' )) {
				if ( ! add_post_meta( $post->ID, '_source', 'field_586fb858a9d34', true ) )  
					update_post_meta( $post->ID, '_source', 'field_586fb858a9d34');
			}
			break;
			
        case 'research':
			if (get_post_meta( $post->ID, 'article_author' )) {
				if ( ! add_post_meta( $post->ID, '_article_author', 'field_586fb8af1c4d6', true ) )  
					update_post_meta( $post->ID, '_article_author', 'field_586fb8af1c4d6');
			}
			if (get_post_meta( $post->ID, 'linkurl' )) {
				if ( ! add_post_meta( $post->ID, '_linkurl', 'field_586fb8d61c4d7', true ) )  
					update_post_meta( $post->ID, '_linkurl', 'field_586fb8d61c4d7');
			}
			if (get_post_meta( $post->ID, 'published' )) {
				if ( ! add_post_meta( $post->ID, '_published', 'field_586fb9d41c4d8', true ) )  
					update_post_meta( $post->ID, '_published', 'field_586fb9d41c4d8');
			}
			if (get_post_meta( $post->ID, 'source' )) {
				if ( ! add_post_meta( $post->ID, '_source', 'field_586fb9ec1c4d9', true ) )  
					update_post_meta( $post->ID, '_source', 'field_586fb9ec1c4d9');
			}
			break;

        case 'training':
			if (get_post_meta( $post->ID, 'Deadline' )) {
				if ( ! add_post_meta( $post->ID, '_Deadline', 'field_586fba7e4e752', true ) )  
					update_post_meta( $post->ID, '_Deadline', 'field_586fba7e4e752');
			}
			if (get_post_meta( $post->ID, 'linkurl' )) {
				if ( ! add_post_meta( $post->ID, '_linkurl', 'field_586fbbb24e753', true ) )  
					update_post_meta( $post->ID, '_linkurl', 'field_586fbbb24e753');
			}
			if (get_post_meta( $post->ID, 'link_title' )) {
				if ( ! add_post_meta( $post->ID, '_link_title', 'field_586fbbca4e754', true ) )  
					update_post_meta( $post->ID, '_link_title', 'field_586fbbca4e754');
			}
			break;
		
        case 'learning':
			if (get_post_meta( $post->ID, 'gallery' )) {
				if ( ! add_post_meta( $post->ID, '_gallery', 'field_586fbeee84b63', true ) )  
					update_post_meta( $post->ID, '_gallery', 'field_586fbeee84b63');
			}
			break;		
    }
}

?>